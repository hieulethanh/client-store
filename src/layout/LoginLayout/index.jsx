import React, { useState } from "react";
import { Button, Checkbox } from "../../components";

const LoginLayout = () => {
  const [state, setState] = useState({
    email: "",
    password: "",
  });

  const [check, isCheck] = useState(false);

  const handleCheck = ({ target }) => {
    isCheck((s) => ({ ...s, [target.name]: !s[target.name] }));
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmitClick = (e) => {
    e.preventDefault();
    console.log({ ...state, check });
  };

  return (
    <div className="container login-layout">
      <div className="row">
        <form>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Email address
            </label>
            <input
              type="email"
              className="form-control"
              aria-describedby="emailHelp"
              onChange={handleChange}
              value={state.email}
              name="email"
            />
            <div id="emailHelp" className="form-text">
              We'll never share your email with anyone else.
            </div>
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Password
            </label>
            <input
              type="password"
              className="form-control"
              onChange={handleChange}
              value={state.password}
              name="password"
            />
          </div>
          <div className="mb-3 form-check">
            <Checkbox
              label="Remember me"
              onChange={handleCheck}
              // disabled
              name="check"
              value={check}
            />
          </div>
          <Button color="primary" type="submit" onClick={handleSubmitClick}>
            Submit
          </Button>
        </form>
      </div>
    </div>
  );
};

export default LoginLayout;
