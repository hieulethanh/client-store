import { useEffect } from "react";

// get currentUser from store

const UseAuth = (props) => {
  const { currentUser } = props;
  useEffect(() => {
    if (!currentUser) props.history.push("/login");
  }, [currentUser]);
  return currentUser;
};

export default UseAuth;
