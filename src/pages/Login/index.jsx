import React from "react";

import { Footer, Header } from "../../components";
import LoginLayout from "../../layout/LoginLayout";

const Login = () => {
  return (
    <div className="login-page">
      <Header />
      <LoginLayout />
      <Footer />
    </div>
  );
};

export default Login;
