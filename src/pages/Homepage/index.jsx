import React from "react";
import { Footer, Header } from "../../components";

import "./styles.scss";

const Homepage = (props) => {
  return (
    <div className="homepage">
      <Header />
      <Footer />
    </div>
  );
};

export default Homepage;
