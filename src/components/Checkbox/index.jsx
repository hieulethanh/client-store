import React from "react";

import "./styles.scss";

const Checkbox = (props) => {
  const { label, name, value, disabled, onChange } = props;
  return (
    <div className="checkbox-component">
      <input
        name={name}
        value={value}
        type="checkbox"
        className="form-check-input"
        onChange={!disabled ? onChange : () => {}}
        disabled={disabled === true ? true : false}
      />
      <label className="form-check-label" htmlFor="exampleCheck1">
        {label}
      </label>
    </div>
  );
};

export { Checkbox };
