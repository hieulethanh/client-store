import React from "react";

import "./styles.scss";

const Button = ({ disabled, onClick, color, children, type }) => {
  return (
    <button
      onClick={!disabled ? onClick : () => {}}
      className={`btn btn-${color}`}
      type={type}
      disabled={disabled === true ? true : false}
    >
      {children}
    </button>
  );
};

export { Button };
