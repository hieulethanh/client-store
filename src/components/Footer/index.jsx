import React from "react";

import "./styles.scss";

const Footer = (props) => {
  return (
    <footer className="footer">
      <div className="wrap container">Client store</div>
    </footer>
  );
};

export { Footer };
