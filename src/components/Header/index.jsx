import React from "react";
import { Link } from "react-router-dom";

import "./styles.scss";

const Header = () => {
  return (
    <header className=" header">
      <div className="container wrap">
        <div className="logo">
          <Link to="/">
            <h3>LOGO</h3>
          </Link>
        </div>
        <div className="col-md-5 search mx-auto">
          <div className="input-group">
            <input
              className="form-control border-end-0 border rounded-pill"
              type="search"
              id="example-search-input"
              placeholder="search"
            />
          </div>
        </div>
        <div className="callToActions">
          <ul>
            <li>
              <Link to="/registration">Registration</Link>
            </li>
            <li>
              <Link to="/login">Login</Link>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
};

export { Header };
