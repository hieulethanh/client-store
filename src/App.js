import React from "react";
import { Route, Switch } from "react-router-dom";

import "./App.css";
import Homepage from "./pages/Homepage";
import Login from "./pages/Login";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" render={() => <Homepage />} />
        <Route path="/login" exact render={() => <Login />} />
      </Switch>
    </div>
  );
}

export default App;
